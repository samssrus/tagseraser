#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
__author__ = "samssrus"
u"""
Скрипт для пакетного удаления id3-тегов из mp3-файлов.
Использование:
python3 eraser.py -f path/to/folder
"""
from mutagen.id3 import ID3, ID3NoHeaderError
import logging
import optparse
import os

# устанавливаем base_path
base_path = os.path.dirname(os.path.realpath(__file__))
# настраиваем лог
logging.basicConfig(
filename=base_path + '/logfile.log',
level=logging.DEBUG,
format='%(asctime)s %(message)s'
)
log = logging.getLogger('app')

options = dict()  # опции скрипта
options["count"] = 0  # количество обработанных файлов

def report(msg):
    print(msg)
    log.info(msg)

def is_mp3(filename):
	u"""функция проверки файла на тип mp3"""
	if filename.endswith(".mp3"):
		return True
	else:
		return False

def erase_tags(root, name):
    u"""

    """
    inputfile = os.path.join(root, name)
    try:
        audio = ID3(inputfile)
        audio.delete()
        msg = "Из файла {} удалены id3-теги".format(inputfile)
    except ID3NoHeaderError:
        msg = "У файла {} нет id3-тегов".format(inputfile)

    report(msg)


def do_it():
    u"""

    """
    for root, dirs, files in os.walk(options["indir"]):
        for name in files:
            if(is_mp3(name)):
                options["count"] += 1
                erase_tags(root, name)
    msg = "Обработано файлов: {}".format(options["count"])
    report(msg)

def main():
	"""
	функция main - начало приложения
	отвечает за прием начальных данных:
	indir - путь к папке с исходными файлами для конвертации
	"""
	p = optparse.OptionParser()
	p.add_option("-f", "--folder", action="store", type="string", dest="indir") #начальный каталог
	opt, args = p.parse_args()

	# установка начального каталога
	if(opt.indir != None):
		options["indir"] = opt.indir

	do_it()

if __name__ == "__main__":
    main()
